const express = require('express');
var router = express.Router();
var config = require('../config');
var User = require('../app/models/user');
var jwt = require('jsonwebtoken');
var auth = require('./auth');
var users = require('./users');

/* GET api listing. */
router.get('/', (req, res) => {
    res.json({
        message: 'Welcome to the coolest API on earth!'
    });
});

router.use(auth);

router.use(function(req, res, next) {
    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, config.secret, function(err, decoded) {
            console.log("err=" + JSON.stringify(err));
            if (err) {
                if (err.name === "TokenExpiredError") {
                    return res.json({
                        success: false,
                        name: err.name,
                        message: err.message
                    });

                } else {
                    return res.json({
                        success: false,
                        message: 'Failed to authenticate token.'
                    });
                }
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });
    } else {
        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
});


router.use(users);


module.exports = router;
