
var router = require('express').Router();
var User = require('../app/models/user');


router.get('/users', function(req, res) {
    console.log("req.decoded=" + JSON.stringify(req.decoded));
    User.find({}, function(err, users) {
        res.json(users);
    });
});

module.exports = router;
